#!/bin/bash
set -v
string="The fat cat sit on the mat and rapidly eat the fat rat"
echo OUT-${string}
echo OUT-${#string}
echo OUT-${string:12}
echo OUT-${string:12:10}
echo OUT-${string#*sit}
echo OUT-${string%sit*}
echo OUT-${string#*the}
echo OUT-${string%the*}
echo OUT-${string##*the}
echo OUT-${string%%the*}



